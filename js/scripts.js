$(document).ready(function(){
    
//    $('#menu li a').pageNav({
//        // offset
//        'scroll_shift': 0,
//        'active_shift': 1000,
//        // active class
//        'active_item':  'active',
//        // animation speed
//        'duration': 900
//    });

    
    $("#menu li a").click(function (){
        var href = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(href).offset().bottom
        }, 1000);
    });
    
    var controller = new ScrollMagic.Controller();
    
    //home
    var home = new ScrollMagic.Scene({
        triggerElement: '#home',
        duration: '100%',
        revers: true, 
        triggerHook: 0,
    })
    .setClassToggle('header, footer, #home', 'active')
    .addIndicators()
    .setPin('#home')
    .addTo(controller); 
    
    //sections
    var sections = ['#section0', '#section1', '#section2', '#section3', '#section4'];
//    $('.section').each(function(){
    $.each(sections, function(index, value){
//        var id = $(this).attr('id');
        console.log(value);
        var section = new ScrollMagic.Scene({
            triggerElement: this,
            duration: '100%',
            revers: true, 
            triggerHook: 0,  
        })
//        .setClassToggle('#'+id+', a[href="#'+id+'"]' , 'active')
        .setClassToggle(value+', a[href="'+value+'"]' , 'active')
        .addIndicators()
        .setPin(this)
        .addTo(controller);
    });
    
//    var pinHome = new ScrollMagic.Scene({
//        triggerelement: '#home',
//        triggerHook: 0,
//        duration: '30%'
//    })
//    .setPin('#home')
//    .addTo(controller);
    
//    var home = new ScrollMagic.Scene({ 
//        triggerElement: '#home',
//        duration: '100%',
//        revers: true, 
//        triggerHook: 0.5,            
//    })
//    .setClassToggle('#home .redStripe', 'fade-in')
//    .addIndicators()
//    .addTo(controller);    
//    
//    var section0 = new ScrollMagic.Scene({ 
//        triggerElement: '#section0',
//        duration: '100%',
//        revers: true, 
//        triggerHook: 0.5, 
//    })
//    .setClassToggle('#section0 .fade-in-start', 'fade-in')
//    .addIndicators()
//    .addTo(controller);    
//    
//    var section1 = new ScrollMagic.Scene({ 
//        triggerElement: '#section1',
//        duration: '100%',
//        revers: true, 
//        triggerHook: 0.5, 
//    })
//    .setClassToggle('#section1 .fade-in-start', 'fade-in')
//    .addIndicators()
//    .addTo(controller);    
//    
//    var section2 = new ScrollMagic.Scene({ 
//        triggerElement: '#section2',
//        duration: '100%',
//        revers: true, 
//        triggerHook: 0.5, 
//    })
//    .setClassToggle('#section2 .fade-in-start', 'fade-in')
//    .addIndicators()
//    .addTo(controller);    
//    
//    var section3 = new ScrollMagic.Scene({ 
//        triggerElement: '#section3',
//        duration: '100%',
//        revers: true, 
//        triggerHook: 0.5, 
//    })
//    .setClassToggle('#section3 .fade-in-start', 'fade-in')
//    .addIndicators()
//    .addTo(controller);    
//    
//    var section4 = new ScrollMagic.Scene({ 
//        triggerElement: '#section4',
//        duration: '100%',
//        revers: true, 
//        triggerHook: 0.5, 
//    })
//    .setClassToggle('#section4 .fade-in-start', 'fade-in')
//    .addIndicators()
//    .addTo(controller);
});